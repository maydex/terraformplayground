# Task 2: From the scratch
## Description:
This simple task demonstrate how to prepare our's first *.tf file with aws provider.

## Goals:
Fill the task2.tf file with certain requirements:
1. AWS provider with version (1.46)
2. region us-west-1
3. alias california

## Recipe:
0. Look at first task or readme to install terraform and inspec.
1. Fill the terraform/task2.tf file (use existing provider block)
2. Execute inspec test
```
cd inspec
inspec exec test.rb -t local://
```
3. You should get a positive result from all of the 3 tests :)

## Docs