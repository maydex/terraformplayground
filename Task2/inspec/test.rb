describe file('task2.tf') do
  its('content') { should match('version(\s+)?=(\s+)?"(\s+)?1.46(\s+)?"') }
  its('content') { should match('region(\s+)?=(\s+)?"(\s+)?us-west-1(\s+)?"') }
  its('content') { should match('alias(\s+)?=(\s+)?"(\s+)?california(\s+)?"') }
end