# Task 1: Hello World
## Description:
The first task is quite simple even if You didn't use terraform/inspec earlier. You need to just follow the instruction.

## Recipe:
0. Download terraform binary [0]
1. Download the code for the repository to yours local pc
2. Create the user with permissions to create the s3 bucket (You can create it via AWS console)[1][2]
3. Execute terraform (You're inside the Task1 directory)
```
cd terraform
terraform init 
terraform plan
terraform apply
```
The first line will download the certain version of aws provider from the internet etc., then we're using plan command which is dry run. At the last step, the terraform will ask You to entry the suffix of the bucket, write a random string, then terraform will create a s3 bucket with name "my-first-terraform-bucket-MYSUFFIX" in "eu-west-1" region (Ireland).

4. Check if the bucket exists on Your aws account (You can use panel or aws cli command provided below)
```
aws s3 ls
```
5. Install inspec:
```
https://downloads.chef.io/inspec
```
6. Execute inspec test
```
cd inspec
inspec exec test.rb -t aws://
```
7. You should get a positive result from all of the 3 tests :)
8. Clear resources created during the task:
```
(You should still) be in inspec directory
cd ../terraform
terraform destroy (double check if terraform will destroy exactly the resources which You want (in this case the s3 bucket in eu-west-1 region))
```

## Docs
[0] https://www.terraform.io/downloads.html

[1] https://docs.aws.amazon.com/IAM/latest/UserGuide/id_users_create.html

[2] https://docs.aws.amazon.com/IAM/latest/UserGuide/reference_policies_examples_s3_rw-bucket.html
