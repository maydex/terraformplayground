provider "aws" {
  version                     = "~> 1.39"
  region                      = "eu-west-1"
  skip_credentials_validation = true
}

variable "s3_bucket_suffix" {
  description = "S3 bucket name suffix"
  default = "bm9ib2t"
}

resource "aws_s3_bucket" "example_s3_bucket" {
  bucket = "my-first-terraform-bucket-${var.s3_bucket_suffix}"
  acl    = "private"

  tags {
    Name        = "test bucket"
    Description = "My first bucket created with terraform"
  }

 provisioner "local-exec" {
    command = "sed -i -e 's/test_bucket/my-first-terraform-bucket-${var.s3_bucket_suffix}/g' ../inspec/test.rb"
  }
}
