describe aws_s3_bucket(bucket_name: 'test_bucket') do
  it { should exist }
  it { should_not be_public }
  its('region') { should eq 'eu-west-1' }
end