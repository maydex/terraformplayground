# terraformPlayground
## Description
Sample tasks, which can help You to learn terraform and (a bit) inspec. The main goal of all tasks is to build certain infrastructure without a certain way.

## Requirements
* Linux/Mac
* Terraform 
* InSpec
* Basic knowledge about the terraform and other tools mentioned earlier.

## Tasks organization
Each directory in this repository contains a single task. The task consists of:
1. Description where can You find information about the task and expected results.
2. A *.tf file(s) where You can put the code.
3. Inspec test which helps You to check if your solution is working.
4. Complete solution how You could solve the puzzle.

## How to add new tasks
You can create and publish new task by simply create MR with new tasks.

## Current task list:
1. Task1 - Hello World (Just call the script and see if the world is burning)
2. Task2 - From the scratch (Create first .tf file with aws provider)
3. Task3 - Providers, providers? providers! (Use multiple providers in single terraform project)
4. Task4 - States Local & Remote (s3) (Create and use different state location)